#include <QCoreApplication>
#include <QFile>
#include <QDebug>
#include <QStringList>
#include <QTextStream>

void write(QString user, QString level)
{
    QString path = QCoreApplication::applicationDirPath();
    path += "/permissionss";

    QFile file;
    QTextStream out;
    out.setDevice(&file);
    file.setFileName(path);
    if (!file.open(QFile::ReadWrite | QFile::Append | QFile::Text))
        qDebug() << "could not open file " << path;

    QString str = user;
    str += "=";
    str += level;
    str += "\n";

    out << str;
    file.close();

}

QStringList read()
{
    QFile file;
    QTextStream read;
    read.setDevice(&file);
    QString path = QCoreApplication::applicationDirPath();
    path += "/permissionss";
    file.setFileName(path);

    if (!file.open(QFile::ReadOnly | QFile::Append | QFile::Text))
        qDebug() << "could not open file " << path;

    QStringList lst;
    read.seek(0);
    while (!read.atEnd())
    {
        //qDebug() << "reading" << read.readLine();
        lst.push_back(read.readLine());
    }
    file.close();

    qDebug() << lst;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    //write("user2", "user");
    //write("admin1","admin");

    read();

    return a.exec();
}
