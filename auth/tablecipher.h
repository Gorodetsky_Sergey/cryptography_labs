#ifndef TABLECIPHER_H
#define TABLECIPHER_H

#include "cipher.h"

#include <QHash>
#include <QStringList>
#include <QDebug>

class TableCipher : public cipher
{
public:
    TableCipher();
    virtual ~TableCipher(){}
    virtual QString encode(QString s)const;
    virtual QString decode(QString s)const;

//private:
    QHash<int, QString> dec;
    QHash<QString, int> enc;
};

#endif // TABLECIPHER_H
