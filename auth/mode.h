#ifndef MODE_H
#define MODE_H



/**
 * Синглтон. Цель класса - хранение данных о правах пользователя.
 *  по мере необходимости добавлять нужные данные о сессии и правах пользователя - сюда.
 *  при логине заполняем этот класс данными. (добавляем поля и сет-гет методы.)
 *  когда нужно узнать уровень допуска пользователя - опрашиваем класс Mode.
 *  вызов синглтона - Mode::instance().%method%
**/

#include <QString>

class Mode
{
public:

    enum Mods{};

    ~Mode() {}

    static Mode &instance()
    {
        static Mode session;
        return session;
    }

    void setUsername(QString s)
    {
        this->username = s;
    }
    QString getUsername()
    {
        return this->username;
    }
private:

    QString username;
    Mode() {}
    Mode(const Mode&) {}
    Mode &operator=(const Mode&) {return *this;}
};

#endif // MODE_H
