#ifndef APP_H
#define APP_H


#include <QObject>
#include <QMainWindow>
#include <QWidget>
#include <QDebug>
#include <QDialog>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QPushButton>

//#include "authentication_dialog.h"
#include "iocredentials.h"
#include "registerd.h"
#include "user.h"

namespace Ui {
class App;
}

class Authentication_Dialog;

class App : public QMainWindow
{
    Q_OBJECT
public:
    explicit App(QWidget* parent);
    ~App();

private slots:
    void on_pushButton_clicked();   

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();    
    void chpwd();

    void on_pushButton_4_clicked();

private:
    Ui::App *ui;
    QString user;
    QString selectedUser;
    RegisterD *reg;
    QLineEdit *pwdChange;
    QLabel *errMsg;
    QDialog *pwdDialog;
    void changePasswd();

};

#endif // APP_H
