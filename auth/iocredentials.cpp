#include "iocredentials.h"


// заменить кустарное шифрование на std::hash и проверять пароли по хэшу.


bool ioCredentials::login(QString loginU, QString passU)
{

   //QString login = loginU.toLower();
   QString login = loginU;
   //QString pass = passU.toLower();
   QString pass = passU;


   loginFile.reset(new QSettings(filepath, QSettings::IniFormat,0));
   permissions.reset(new QSettings(permissionsPath, QSettings::IniFormat,0));
   salts.reset(new QSettings(saltPath, QSettings::IniFormat,0));

   permissions->sync();
   salts->sync();
   loginFile->sync();

   QString hashedPass = loginFile -> value(login,"error").toString();
   QString usrCredentials = pass + salts->value(login,"error").toString();
   loginFile -> sync();

try {
  if (login.isEmpty() || pass.isEmpty())
  {throw(qt_error_string(-1));}


  if (hashedPass != "error")
  {
      QString enteredPasswordHash = coder->encode(usrCredentials);


     if (enteredPasswordHash == hashedPass)
     {

         // успешно залогиниться!!!         
         qDebug() << "login succesful";
         QString msg("Авторизация прошла успешно, ");
         msg += loginU;
         msg += "!";
         this->warningMsg->setText(msg);
         this->warningMsg->setStyleSheet("color: green");

        //User::Password = "123";
         //Mode::instance().setUsername(login);
         User::Username = login;
         User::Password = pass;
         User::Salt = salts->value(login,"error").toString();
         User::perm = permissions->value(login,"error").toString();
//         User::instance().setUsername(login);
//         User::instance().setPasswd(pass);
//         User::instance().setSalt(salts->value(login,"error").toString());
//         User::instance().setPermissions(permissions->value(login,"error").toString());
         return true;
     }
     else
     {         
         qDebug() << "login failed";
         this->warningMsg->setText("Неправильный логин или пароль");
         this->warningMsg->setStyleSheet("color: red");
         // написать, что неправильный логин и пассворд.

         return false;
     }

  }
  else
  {
      qDebug() << "wrong login";
      this->warningMsg->setText("Неправильный логин или пароль");
      this->warningMsg->setStyleSheet("color: red");
      //throw error here;

      return false;
  }
    }//try
  catch(...)
  {
        qDebug() << "caugh!";
        this->warningMsg->setText("Ошибка");
        this->warningMsg->setStyleSheet("color: red");
  }

    return false;
}

bool ioCredentials::loginInUse(const QString &login)
{
    loginFile.data()->sync();
    return loginFile.data()->contains(login);
}

bool ioCredentials::newUser(QString loginU, QString passwordU, QString permission)
{
   //QString login = loginU.toLower();
   //QString pass = passwordU.toLower();
   QString login = loginU;
   QString pass = passwordU;

   QString saltBuilder = QString::number(QTime::currentTime().hour());
   saltBuilder += QString::number(QTime::currentTime().minute());
   saltBuilder += QString::number(QTime::currentTime().second());
   saltBuilder += QString::number(QTime::currentTime().msec());
   saltBuilder += saltCount++;

   //qsrand(saltBuilder.toInt());
   //QString salt = QString::number(qrand());
   QString salt = saltBuilder;
   salt = salt.left(salt.length()-1);
   pass += salt;



   QString hashedPass = coder->encode(pass);
   qDebug() << hashedPass;

   loginFile.reset(new QSettings(filepath, QSettings::IniFormat,0));
   loginFile->sync();
   loginFile->setValue(login,hashedPass);
   loginFile->sync();

   permissions.reset(new QSettings(permissionsPath, QSettings::IniFormat,0));
   permissions->sync();
   permissions->setValue(login,permission);
   permissions->sync();

   salts.reset(new QSettings(saltPath, QSettings::IniFormat,0));
   salts->sync();
   salts->setValue(login,salt);
   salts->sync();

   return true;

}

QString ioCredentials::checkPermission(const QString &username)
{
    permissions.reset(new QSettings(permissionsPath, QSettings::IniFormat,0));
    permissions->sync();
    return permissions->value(username,"err").toString();
}

QStringList ioCredentials::userList()
{
    return this->loginFile->allKeys();
}

void ioCredentials::chPasswd(QString &username, QString &newPasswd)
{
    salts.reset(new QSettings(saltPath, QSettings::IniFormat,0));
    loginFile.reset(new QSettings(filepath, QSettings::IniFormat,0));
//    QDialog *passInput = new QDialog();
//    QVBoxLayout *l = new QVBoxLayout(passInput);
//    l->addWidget(new QLineEdit(passInput));
//    passInput->setLayout(l);
    QString saltBuilder = QString::number(QTime::currentTime().hour());
    saltBuilder += QString::number(QTime::currentTime().minute());
    saltBuilder += QString::number(QTime::currentTime().second());
    saltBuilder += QString::number(QTime::currentTime().msec());
    saltBuilder += saltCount++;

    //qsrand(saltBuilder.toInt());
    //QString salt = QString::number(qrand());
    QString salt = saltBuilder;
    salt = salt.left(salt.length()-1); //вынести в функцию и оптимизировать.

    QString pass = newPasswd;
    pass += salt;
    QString hashedPass = coder->encode(pass);

    loginFile->sync();
    loginFile->remove(username);
    loginFile->sync();
    loginFile->setValue(username,hashedPass);

    salts->sync();
    salts->remove(username);
    salts->sync();
    salts->setValue(username,salt);
    salts->sync();
}

void ioCredentials::addPermissions(QString user, QString userLevel)
{
    QString path = QApplication::applicationDirPath() + "/permissions.ini";
    permissions.reset(new QSettings(path,QSettings::IniFormat,0));
    permissions.data()->sync();
    permissions.data()->setValue(user,userLevel);
    permissions.data()->sync();
}


