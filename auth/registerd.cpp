#include "registerd.h"
#include "ui_RegisterD.h"

#include "iocredentials.h"


RegisterD::RegisterD(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RegisterD)
{
    ui->setupUi(this);

}

RegisterD::~RegisterD()
{

}


//void RegisterD::on_buttonBox_accepted()
//{

//}

void RegisterD::on_pushButton_clicked()
{
    bool acceptDialog = true;
    bool passMatch;
    if (ui->login->text().isEmpty()  &&
        ui->passwd->text().isEmpty() &&
        ui->passwd2->text().isEmpty())
    {
        ui->warning->setText("Введите все необходимые данные, помеченные *");
        acceptDialog = false;
        return;
    }


    if (ui->passwd->text() == ui->passwd2->text())
    {
        ui->passwdMsg->setText("Одинаковы!");
        ui->passwdMsg->setStyleSheet("color: green");
        passMatch = true;
    }
    else
    {
        ui->passwdMsg->setText("Пароли разные");
        ui->passwdMsg->setStyleSheet("color: red");
        acceptDialog = false;
        passMatch = false;
    }

    QString mail = ui->email->text();
    QString login = ui->login->text();
    if (ioCredentials::instance().loginInUse(login))
    {
        acceptDialog = false;
        ui->loginLabel->setText("Логин уже используется");
        ui->loginLabel->setStyleSheet("color: red");
    }
    else
    {
        ui->loginLabel->setText("Логин доступен");
        ui->loginLabel->setStyleSheet("color: green");
    }


    if (passMatch){
        if (!isPassStrong(ui->passwd->text()))
            acceptDialog = false;

    }

    if (acceptDialog)
    {
        qDebug() << "accepted";
        QString permission;
        if(ui->adminBtn->isChecked())
            permission = "a";
         else
            permission = "u";


        //if (QObject::)
        ioCredentials::instance().newUser(ui->login->text(),ui->passwd->text(), permission);
        this->accept();
    }
    else
    {
        qDebug() << "not accepted";
    }
}

bool RegisterD::isPassStrong(const QString &s)
{
    int lenght = s.length();

    int digitCounter = 0;
    int upperCounter = 0;
    int lowerCounter = 0;

    bool isPassOK = true;

    if (lenght > 22 || lenght < 6)
    {
        isPassOK = false;
    }
    for (const QChar &c : s)
    {
        if (c.isDigit())
            digitCounter++;//use 'continue'
        if(c.isLower())
            lowerCounter++;
        if(c.isUpper())
            upperCounter++;

    }

    if (digitCounter == 0 || lowerCounter == 0 || upperCounter == 0)
    {
        isPassOK = false;
        ui->warning->setText("Пароль должен иметь 6-22 символов, состоять из цифр, заглавных,строчных букв. ");
    }

    return isPassOK;


}

void RegisterD::on_pushButton_2_clicked()
{
    this->reject();
}
