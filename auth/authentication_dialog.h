#ifndef AUTHENTICATION_DIALOG_H
#define AUTHENTICATION_DIALOG_H

#include <QDialog>

#include "app.h"
#include "iocredentials.h"

namespace Ui {
class Authentication_Dialog;
}

class Authentication_Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Authentication_Dialog(QWidget *parent = 0);
    ~Authentication_Dialog();

private:
    Ui::Authentication_Dialog *ui;
    QPushButton *shadowBG;
    QDialog *reg;
    QMainWindow *app;


private slots:
    void clearPasswd();
    void doClose();
    void on_authButton_clicked();
    void on_RegisterButton_clicked();
};

#endif // AUTHENTICATION_DIALOG_H
