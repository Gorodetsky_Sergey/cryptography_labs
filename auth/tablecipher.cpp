#include "tablecipher.h"

TableCipher::TableCipher()
{
    QString tmp = "й ц у к е н г ш щ з х ъ ф ы в а п р о л д ж э я ч с м и т ь б ю ё"
                  " z x c v b n m a s d f g h j k l q w e r t y u i o p 0 9 8 7 6 5 4 3 2 1 - _ ";

    QStringList alphabet = tmp.split(" ");
    int i = 1;
    for (QString s : alphabet)
    {
        enc.insert(s,i);
        dec.insert(i,s);
        i++;
    }
}

QString TableCipher::encode(QString s) const
{
   QString tmp = s.toLower();
   QString res;
   for (auto a : tmp)
   {
      res += QString::number(enc.value(a));
      res +="o";
   }

   return res;
}

QString TableCipher::decode(QString s) const
{
    QString tmp = s.toLower();
    QStringList q = tmp.split("o");
    QString res;

    for (QString ss : q )
    {
        //qDebug() << ss;
        res += dec.value(ss.toInt());
    }


//    for (QChar c : tmp)
//    {

//        QString t = c;
//         if (t != " "){
//        res += dec.value(t.toInt());
//        }
//    }
    return res;
}

