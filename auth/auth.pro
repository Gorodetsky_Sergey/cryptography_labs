#-------------------------------------------------
#
# Project created by QtCreator 2015-12-03T16:57:09
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = auth
TEMPLATE = app
CONFIG += c++11


SOURCES += main.cpp\
        authentication_dialog.cpp \
    iocredentials.cpp \
    cipher.cpp \
    tablecipher.cpp \
    mode.cpp \
    stdhash.cpp \
    registerd.cpp \
    app.cpp \
    sha256.cpp \
    user.cpp

HEADERS  += authentication_dialog.h \
    iocredentials.h \
    cipher.h \
    tablecipher.h \
    mode.h \
    stdhash.h \
    registerd.h \
    app.h \
    sha256.h \
    user.h

FORMS    += authentication_dialog.ui \
    RegisterD.ui \
    app.ui

DISTFILES +=
