#ifndef STDHASH_H
#define STDHASH_H

#include "cipher.h"

#include <QDebug>

class stdHash : public cipher
{
public:
    stdHash();
   virtual ~stdHash(){}

    virtual QString encode(QString s)const;
    virtual QString decode(QString s)const;
};

#endif // STDHASH_H
