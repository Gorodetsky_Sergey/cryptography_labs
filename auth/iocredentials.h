#ifndef IOCREDENTIALS_H
#define IOCREDENTIALS_H

#include "cipher.h"
#include "tablecipher.h"
#include "mode.h"
#include "stdhash.h"
#include "sha256.h"
#include "user.h"

#include <QSettings>
#include <QLabel>
#include <QApplication>
#include <QTime>
#include <cstdlib>




class ioCredentials //: public QObject
{
  //  Q_OBJECT
public:
    ~ioCredentials(){}

    static ioCredentials &instance()
    {
        static ioCredentials session;
        return session;
    }

    void setFilePath(QString s)
    {
        filepath = s;
        loginFile.reset(new QSettings(filepath, QSettings::IniFormat,0));
    }

    bool login(QString loginU, QString passU);
    bool loginInUse(const QString &login);
    void setLabel(QLabel *ql){this->warningMsg = ql;}
    bool newUser(QString loginU, QString passwordU, QString permission); // loginU - login entered by User.
    QString checkPermission(const QString &username);
    QStringList userList();
    QString personalData(QString &username);
    void chPasswd(QString &username, QString &newPasswd);

private:
    unsigned int saltCount = 0;
    QLabel *warningMsg;
    QString filepath;
    QString saltPath;
    QString permissionsPath;
    cipher *coder;
    QScopedPointer<QSettings> loginFile;
    QScopedPointer<QSettings> permissions;
    QScopedPointer<QSettings> salts;
    QScopedPointer<QSettings> userData;

    ioCredentials()
    {
        coder = new sha256(); // стратегию менять тут.
        saltPath = QApplication::applicationDirPath() + "/salts.ini";
        permissionsPath = QApplication::applicationDirPath() + "/permissions.ini";
    }
    ioCredentials(const ioCredentials&) {}
    ioCredentials &operator=(const ioCredentials&) {return *this;}

    void addPermissions(QString user, QString userLevel);

};

#endif // IOCREDENTIALS_H
