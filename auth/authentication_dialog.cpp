#include "authentication_dialog.h"
#include "ui_authentication_dialog.h"


#include <QDebug>
#include <QApplication>
#include <QDesktopWidget>

#include "registerd.h"

Authentication_Dialog::Authentication_Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Authentication_Dialog)
{
    ui->setupUi(this);    

    Qt::WindowFlags flags = 0;

   // flags |= Qt::FramelessWindowHint;
    flags |= Qt::WindowStaysOnTopHint;
    setWindowFlags(flags);


    shadowBG = new QPushButton();  //удаляется вручную в методе doClose
    shadowBG->setAutoFillBackground(1);
    shadowBG->setPalette(QColor(0,0,0));
    shadowBG->setWindowOpacity(0.5);
    shadowBG->showFullScreen();
    connect(shadowBG, SIGNAL(clicked()), SLOT(doClose()));
    connect(this,SIGNAL(rejected()),SLOT(doClose()));    

    this->move(QApplication::desktop()->screen()->rect().center() - this->rect().center());

   ioCredentials::instance().setFilePath(QApplication::applicationDirPath() + "/cfg.ini");
   ioCredentials::instance().setLabel(ui->warningLabel);

   ui->RegisterButton->hide();
  // qDebug() << QApplication::applicationDirPath() + "/cfg.ini";


}

Authentication_Dialog::~Authentication_Dialog()
{
    qDebug() << "success";
    delete ui;
    //();
}

void Authentication_Dialog::clearPasswd()
{
    ui->Password->clear();
    this->show();
}

void Authentication_Dialog::doClose()
{
    close();
    shadowBG->deleteLater();
}



void Authentication_Dialog::on_authButton_clicked()
{
    try{
      bool auth = ioCredentials::instance().login(ui->Login->text(),ui->Password->text());
      if (auth)
      {
          //this->hide();
          this->app = new App(this);
          connect(app, SIGNAL(destroyed()), this, SLOT(clearPasswd()));
          app->show();
          this->hide();


      }
    }//try
    catch (const char* err){qDebug() << err;}
    catch(QString s){qDebug() << s;}
    catch(...){qDebug() << "caught";}
     // qDebug() << "CLICKED WITH:  " << ui->Login->text();
}

void Authentication_Dialog::on_RegisterButton_clicked()
{
    qDebug() << "clicked";
    //ioCredentials::instance().newUser("login2","pass2");
    reg = new RegisterD(this);
    reg->show();

}
