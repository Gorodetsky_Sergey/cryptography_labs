#ifndef SHA256_H
#define SHA256_H

#include "cipher.h"
#include <QCryptographicHash>

class sha256 : public cipher
{
public:
    sha256();
    virtual ~sha256();

    virtual QString encode(QString s)const;
    virtual QString decode(QString s)const;
};

#endif // SHA256_H
