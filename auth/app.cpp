#include "app.h"
#include "ui_app.h"


App::App(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::App)
{
    ui->setupUi(this);
    QString permission = "";

    this->setAttribute(Qt::WA_DeleteOnClose);



    //qDebug() << User::instance().permissions() << User::instance().username;
    //qDebug() << User::instance().perm << User::instance().Username;
    if (User::perm == "u")
    {
        permission = "user";
        ui->pushButton->setDisabled(true);
        ui->pushButton_3->setDisabled(true);
        ui->pushButton_4->setDisabled(true);
    }
    else if (User::perm == "a")
        permission = "admin";
    else
        throw("Access rights violation");

    ui->permission->setText(permission);
    ui->welcome_label->setText("Welcome, " + User::Username);

    //ui->listWidget->addItem("test");
    //ui->listWidget->addAction(new QAction("action!!!."));
    for (QString &s : ioCredentials::instance().userList())
    {
        QString entry = s;
        entry += "    acess level: ";
        QString perm;
        if (ioCredentials::instance().checkPermission(s) == "u")
            perm = "user";
        else
            perm = "admin";

        entry += perm;
        ui->listWidget->addItem(entry);
    }

}

App::~App()
{
    qDebug() << "deleted app";
}


void App::on_pushButton_clicked()
{
    reg = new RegisterD(this);
    reg->show();
    this->hide();
    connect(reg,SIGNAL(accepted()),this,SLOT(show()));
    connect(reg,SIGNAL(rejected()),this,SLOT(show()));
}

void App::on_pushButton_2_clicked()
{
    this->close();
}

void App::on_pushButton_3_clicked()
{//change password
    this->changePasswd();
}

void App::changePasswd()
{
    //QString userName = ui->listWidget->currentItem()->text().split("").at(0);
    const QString msg = "Enter new password";
    //qDebug() << userName.split(" ").at(0);
    selectedUser = ui->listWidget->currentItem()->text().split(" ").at(0);
    QDialog *passInput = new QDialog(this);
    pwdDialog = passInput;
    QVBoxLayout l;
    l.addWidget(new QLabel(msg,passInput));
    this->pwdChange = new QLineEdit(passInput);
    l.addWidget(pwdChange);
    QPushButton *acceptBtn = new QPushButton("Accept",passInput);
    l.addWidget(acceptBtn);
    errMsg = new QLabel(this);
    errMsg->setText("");
    errMsg->hide();
    l.addWidget(errMsg);
    connect(acceptBtn, SIGNAL(clicked(bool)), this, SLOT(chpwd()));
    passInput->setLayout(&l);
    passInput->show();
    passInput->setModal(true);
    passInput->raise();
}

void App::chpwd()
{
    QString s = pwdChange->text();
    if (s.length() < 6 || s.length() > 22)
    {
        errMsg->setText("6-22 symbols required");
        errMsg->show();
        //errMsg->setGeometry(errMsg->x(),errMsg->y(),errMsg->height(),errMsg->width()+30);
        errMsg->setStyleSheet("color: red");
        return;
    }
    int chars = 0;
    int nums = 0;
    int uppers = 0;
    for (QChar &c : s)
    {
        if (c.isDigit())
            nums++;
        if (c.isLetter())
            chars++;
        if(c.isUpper())
            uppers++;
    }
    if (chars == 0 || nums == 0 || uppers == 0)
    {
        errMsg->setText("digits,letters and upper-case letter required");
        errMsg->show();
        //qDebug() << errMsg->width();
       //errMsg->setGeometry(errMsg->x(),errMsg->y(),errMsg->height(),errMsg->width()+30);
        errMsg->setStyleSheet("color: red");
        return;
    }
    ///s.toLower();
    ioCredentials::instance().chPasswd(selectedUser, s);
    this->pwdDialog->close();
    this->pwdDialog->deleteLater();
}

void App::on_pushButton_4_clicked() //change permission
{

}
