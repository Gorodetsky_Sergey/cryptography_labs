#ifndef REGISTERD_H
#define REGISTERD_H

#include <QObject>
#include <QDialog>
#include <QWidget>

namespace Ui {
class RegisterD;
}

class RegisterD : public QDialog
{
    Q_OBJECT
public:
    explicit RegisterD(QWidget* parent);
    ~RegisterD();
private slots:
    //void on_buttonBox_accepted();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::RegisterD *ui;
    bool isPassStrong(const QString &s);
};

#endif // REGISTERD_H
