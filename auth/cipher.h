#ifndef CIPHER_H
#define CIPHER_H

#include <QString>

//Паттерн стратегия. Для повышения гибкости выбора способа шифрования.
class cipher
{
public:
    cipher();
   virtual ~cipher(){}

    virtual QString encode(QString s)const =0;
    virtual QString decode(QString s)const =0;
};

#endif // CIPHER_H
